#!/usr/bin/env python

from dash_extensions.enrich import Output, Input, State, DashProxy, MultiplexerTransform, dcc, html
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import dash_gif_component as gif
from pathlib import Path
import geopandas as gpd
import pandas as pd
import unidecode
import base64
import plotly
import json
import dash
import sys


sys.path.append('../src')
import data_sources as dts
import plots as pts

import warnings
warnings.filterwarnings('ignore')



############### Funcion para leer imagenes #################
def b64_image(image_filename):
    with open(image_filename, 'rb') as f:
        image = f.read()
    return 'data:image/png;base64,' + base64.b64encode(image).decode('utf-8')

############## Generador del indicador de cambio en las tarjetas de poblacion y uso de suelo ####################

def indicator_generator(amount):
    if amount == 'No aplica.':
        return None
    else:
        amount = round(amount*100, 2)
    
        fig = go.Figure(
            go.Indicator(
                mode="delta",
                value=amount,
                delta={"reference": 100, "relative": True},
            )
        )
        fig.update_layout(margin=dict(l=0, r=0, t=0, b=0),
                         paper_bgcolor="rgba(0,0,0,0)")
        
        return dcc.Graph(figure = fig, style={"height": 50, "width": 100})


##################### Carga de datos #################

logo = 'BID.png'

data_path = Path('../data')
cities_fua = gpd.read_file(data_path / 'output' / 'cities' / 'cities_fua.gpkg')
cities_uc = gpd.read_file(data_path / 'output' / 'cities' / 'cities_uc.gpkg')


############### Estilos basicos de CSS  #################
# Color palette for IDB
color_palette = ['#292D73', '#2F5C97', '#326D8D','#707788', '#C5D0DF']

# Header stylesheet CSS
HEADER_STYLE = {'text-align':'center',
                'color': '#404756',
                'margin-bottom': '2rem',
                'font-size': '30px'
}

# Sidebar stylesheet CSS
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": '#404756', #"#f8f9fa"
}

# Main content stylesheet CSS
CONTENT_STYLE = {
    "margin-left": "16rem",
    "top": 0,
    "left": 0,
    "bottom": '50rem',
    "height": '100%',
    #"margin-right": "2rem",
    "padding": "2rem 4rem", #top left-right
    'background-color': '#FBFBFB',
}

# Label style in the dropdowns
TOGGLE_STYLE = {
    'background-color': '#FBFBFB',
    'border-color' : '#FBFBFB',
    'color': 'gray'
}

# Dropdown menu stylesheet CSS
DROPDOWN_MENU = {"margin-left": "16rem",
                 "margin-bottom": "-2rem",
                 "background-color": "#FBFBFB", 
                 "display": "flex",
                 "padding-top": "35px",
                 "padding-left": "60px",
}

SUBTITLE_YEAR = {'margin-top': '-2rem',
                 'margin-bottom': '2rem'}

############### Dropdown de pais y cuidad #################
country_dropdown=dcc.Dropdown(options=[{'label': country, 'value': country} for country in cities_fua.country.unique()],
                              value='Argentina',
                              id='cou-dro', className='dropdown-style'
                             )


city_dropdown=dcc.Dropdown(options=[{'label': city, 'value': city} for city in cities_fua.city.unique()],
                           value='San Juan',
                           id='cit-dro', className='dropdown-style'
                          )

################# Dropdowm del menu principal ################

menu = html.Div([dbc.Row([
    dbc.Col(
        dbc.DropdownMenu(
            children=[
                dbc.DropdownMenuItem("Urbanización", href="/pas/urb"),
                dbc.DropdownMenuItem("Población", href="/pas/pop"),
                dbc.DropdownMenuItem("Cobertura de suelo", href="/pas/lan"),
            ],
            label="Pasado",
            align_end=False,
            toggle_style=TOGGLE_STYLE
        )),
         
    dbc.Col(
        dbc.DropdownMenu(
            children=[
                dbc.DropdownMenuItem("Urbanización", href="/pre/urb"),
                dbc.DropdownMenuItem("Población", href="/pre/pop"),
                dbc.DropdownMenuItem("Cobertura de suelo", href="/pre/lan"),
            ],
            label="Presente",
            align_end=False,
            toggle_style={'background-color': '#FBFBFB',
                         'border-color' : '#FBFBFB',
                         'color': 'gray'},
            className='menu-items'
        )),
    dbc.Col(
        dbc.DropdownMenu(
            children=[
                dbc.DropdownMenuItem("Crecimiento futuro", href="/fut")
            ],
            label="Futuro",
            align_end=False,
            toggle_style=TOGGLE_STYLE
        )),
])], style=DROPDOWN_MENU)


############### Sidebar con logo y dropdowns de filtrado de país #################
sidebar = html.Div([html.Img(src=b64_image(logo),
                            style={ 'width': '100%'}),
                    
                    html.Hr(),
                    
                    dbc.Col([dbc.Label("Filtrar por país"),
                                 country_dropdown], className='pais-style'
                                #width=6
                           ),
                    
                    dbc.Row([dbc.Label("Filtrar por cuidad"),
                                 city_dropdown], className='pais-style'
                                #height=6
                           ),
                    
                    html.Button('Submit', id='submit-button', 
                                n_clicks=0, className='button-style-sub',
                                #children='Submit'
                               ),
                               
                    
                   ],
                   
                   style=SIDEBAR_STYLE)

############### Content y Header #################
line = html.Div(html.Hr(), className = 'content-style')

header = html.Div([line, 
                    html.Div(
                    id='header',
                    style = HEADER_STYLE)
                    ])

content = html.Div([
                    header,
                    html.Div(
                             id='content', 
                             className = 'content-style'),
                    ],
                   style=CONTENT_STYLE
                    )


############### Creacion de la app y uso de Darkly theme ###############
app = DashProxy(external_stylesheets=[dbc.themes.DARKLY], transforms=[MultiplexerTransform()])


app.layout = html.Div([dcc.Location(id="times"), dcc.Store(id='store_data', storage_type='session'), 
                       menu, 
                       sidebar,
                       content], 
                     )


############################# Callbacks ############################

@app.callback(Output('cit-dro', 'options'),
               Output('cit-dro', 'value'),
              Input('cou-dro', 'value'))
def filter_city(cou):
    df_cou = cities_fua[cities_fua.country==cou]
    df_cou = df_cou.city.unique()
    df_cou = list(df_cou)
    df_cou.sort()
    options = [{'label': city, 'value': city} for city in df_cou]
    return options, options[0]['value']


@app.callback(Output('store_data', 'data'),
              Output('header', 'children'),
              Output('content', 'children'),
             Input('submit-button', 'n_clicks'),
             State('cou-dro','value'),
            State('cit-dro','value'),
             prevent_initial_call=True)

def load_data(n_clicks, country, city):
    if n_clicks > 0:
        
        dcc.Store(id='store_data', storage_type='session', clear_data=True)

        children = [html.H1('{0}, {1}'.format(country, city), 
                            style=HEADER_STYLE)]
        
        pts.download_data(country, city)
        
        figs = pts.urb_pop_graph(country, city)
        cards_pop_urb = pts.urb_pop_cards(country, city)
        density_txt = pts.density_wp(country, city)
        urbanization_txt = pts.urbanization_txt(country, city)
        
        area, pre_bar_land, card_land_txt, card_land_num = pts.land_graph(country, city)
        
        pre_gisa = pts.pre_gisa(country, city)
        pre_map_pop = pts.present_map('WorldPop', country, city)
        pre_map_land = pts.present_map('DynamicWorld', country, city)
        
        gisa_slider = pts.gisa_slider(country, city)
        landsat_gif = pts.landsat_animation(city, country)
        
        sleuth_fast_map = pts.sleuth_map(country, city, 'sleuth_fast_2040.tif', 'Crecimiento Rápido 2040')
        sleuth_usual_map = pts.sleuth_map(country, city, 'sleuth_usual_2040.tif', 'Crecimiento Habitual 2040')
        sleuth_slow_map = pts.sleuth_map(country, city, 'sleuth_slow_2040.tif', 'Crecimiento Lento 2040')
        
        sleuth_graph = pts.sleuth_graph()
        
        json_figs = json.dumps({'figs': figs,
                     'cards_pop_urb': cards_pop_urb,
                    'card_land_txt': card_land_txt,
                    'card_land_num': card_land_num,
                    'density_txt': density_txt,
                    'urbanization_txt': urbanization_txt,
                     'area': area.to_json(),
                    'pre_bar_land': pre_bar_land.to_json(),
                     'pre_gisa': pre_gisa.to_json(),
                     'pre_map_pop': pre_map_pop.to_json(),
                    'pre_map_land': pre_map_land.to_json(),
                    'gisa_slider': gisa_slider,
                    'landsat_gif' : landsat_gif, 
                    'sleuth_fast_map': sleuth_fast_map.to_json(),
                     'sleuth_usual_map': sleuth_usual_map.to_json(),
                     'sleuth_slow_map': sleuth_slow_map.to_json(),
                    'sleuth_graph': sleuth_graph.to_json(),
                     
                     
            
        })
        
        return json_figs, children, None
    
    else:
        return PreventUpdate
        
    
        
@app.callback(Output('content', 'children'),
             State('store_data', 'data'),
             Input('times', 'pathname'),
             prevent_initial_call=True)
def render_content(json_data, pathname):
    if json_data is None:
        raise PreventUpdate
        
    data = json.loads(json_data)
    
    figs_json = data['figs']
    figs = [plotly.io.from_json(i) for i in figs_json]
    
    cards_pop_urb = data['cards_pop_urb']
    card_land_txt = data['card_land_txt']
    card_land_num = data['card_land_num']
    density_txt = data['density_txt']
    urbanization_txt = data['urbanization_txt']
    
    area = plotly.io.from_json(data['area'])
    
    pre_gisa = plotly.io.from_json(data['pre_gisa'])
    pre_map_pop = plotly.io.from_json(data['pre_map_pop'])
    pre_bar_land = plotly.io.from_json(data['pre_bar_land'])
    pre_map_land = plotly.io.from_json(data['pre_map_land'])
    
    
    gisa_slider = plotly.io.from_json(data['gisa_slider'])
    landsat_gif = data['landsat_gif']
    
    
    sleuth_fast_map = plotly.io.from_json(data['sleuth_fast_map'])
    sleuth_usual_map = plotly.io.from_json(data['sleuth_usual_map'])
    sleuth_slow_map = plotly.io.from_json(data['sleuth_slow_map'])
    
    sleuth_graph = plotly.io.from_json(data['sleuth_graph'])
     
        
    if pathname == '/pas/urb':
        
        pas_urb = [dbc.Row([html.Div([html.H3('(1990-2019)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                   dbc.Row([dbc.Col(dcc.Graph(figure=gisa_slider),
                                    style={"margin-left": "10rem", "margin-bottom": "1rem"}
                                   )
                           ]),
                   
                   dbc.Row([dbc.Col(gif.GifPlayer(gif=b64_image(landsat_gif), still=None),
                                   style={"margin-left": "10rem"})]),
        
                   dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style', width=10),
                            dbc.Col(html.A('Landsat,', 
                                     href='https://developers.google.com/earth-engine/datasets/catalog/landsat',
                                   target="_blank", className='src-link-style'),
                                   width=1),
                            dbc.Col(html.A('GISA', 
                                     href='https://samapriya.github.io/awesome-gee-community-datasets/projects/gisa/',
                                   target="_blank", className='src-link-style'),
                                   width=1)],
                          className='src-style-paur')

                       ]
        return pas_urb
    elif pathname == '/pas/pop':
        pas_pop = [dbc.Row([html.Div([html.H3('(2000-2020)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                    dbc.Row([dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Población",
                                                          className="card-title"),
                                               indicator_generator(cards_pop_urb['diff_pop']),
                                                  html.P(str(cards_pop_urb['pop_txt']),
                                                         className="card-text")])], 
                                color="#005073", inverse=True),
                                    width=4
                               ),
                        dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Urbanización",
                                                          className="card-title"),
                                               indicator_generator(cards_pop_urb['diff_urb']),
                                                  html.P(str(cards_pop_urb['urb_txt']),
                                                         className="card-text")])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                        dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Densidad",
                                                          className="card-title"),
                                               indicator_generator(cards_pop_urb['diff_den']),
                                                  html.P(str(cards_pop_urb['den_txt']),
                                                         className="card-text")])],
                                color="secondary", inverse=True),
                                    width=4
                               )
                       ]),
                html.Hr(),
                   
               dbc.Row([dbc.Col(dcc.Graph(figure=figs[0]),
                               width=4
                               ),
                        dbc.Col(dcc.Graph(figure=figs[1]),
                               width=4
                               ),
                        dbc.Col(dcc.Graph(figure=figs[2]),
                               width=4
                               )
                       ]),
                dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                         dbc.Col(html.A('WorldPop,', 
                                     href='https://www.worldpop.org',
                                   target="_blank", className='src-link-style')),
                         dbc.Col(html.A('GISA', 
                                     href='https://samapriya.github.io/awesome-gee-community-datasets/projects/gisa/',
                                   target="_blank", className='src-link-style')),
                           ],
                          className='src-style')
                 ]
        return pas_pop
    elif pathname == '/pas/lan':
        pas_lan = [dbc.Row([html.Div([html.H3('(2016-2021)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                   dbc.Row([dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Urbanización",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Urbanización']),
                                               html.P(card_land_txt['Urbanización'],
                                                      className="card-text")
                                                  ])],
                                color="#801a00", inverse=True),
                                    width=4
                               ),
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Cultivos",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Cultivos']),
                                               html.P(card_land_txt['Cultivos'],
                                                      className="card-text")
                                                  ])], 
                                color='#264d00', inverse=True),
                                    width=4
                               ),
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Árboles",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Árboles']),
                                               html.P(card_land_txt['Árboles'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                            
                       ], style={'margin-bottom': '1rem'}),
                   
                   dbc.Row([dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Vegetación inundada",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Vegetación inundada']),
                                               html.P(card_land_txt['Vegetación inundada'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                           dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Césped/Pasto",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Césped/Pasto']),
                                               html.P(card_land_txt['Césped/Pasto'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Arbusto y matorral",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Arbusto y matorral']),
                                               html.P(card_land_txt['Arbusto y matorral'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                       ], style={'margin-bottom': '1rem'}),
                   
                   dbc.Row([
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Agua",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Agua']),
                                               html.P(card_land_txt['Agua'],
                                                      className="card-text")
                                                  ])], 
                                color="dark", inverse=True),
                                    width=4
                               ),
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Descubierto",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Descubierto']),
                                               html.P(card_land_txt['Descubierto'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                            dbc.Col(dbc.Card(
                                [dbc.CardBody([html.H5("Nieve y hielo",
                                                          className="card-title"),
                                               indicator_generator(card_land_num['Nieve y hielo']),
                                               html.P(card_land_txt['Nieve y hielo'],
                                                      className="card-text")
                                                  ])],
                                color="dark", inverse=True),
                                    width=4
                               ),
                       ], style={'margin-bottom': '2rem'}),
                   
                   dbc.Row([dbc.Col(dcc.Graph(figure=area),
                               #width=10
                                   )]),
                   
                   dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                            dbc.Col(html.A('Dynamic World V1', 
                                     href='https://developers.google.com/earth-engine/datasets/catalog/GOOGLE_DYNAMICWORLD_V1#description',
                                   target="_blank", className='src-link-style'))
                           ],
                          className='src-style')
                 ]
        return pas_lan
    elif pathname == '/pre/urb':
        pre_urb = [dbc.Row([html.Div([html.H3('(2021)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                   
                   dbc.Row([html.Div(html.H5(urbanization_txt, style = {'text-align':'center'})
                                    )
                           ], style={'margin-bottom': '2rem', 
                                                 'text-align': 'center',
                                                'align-content': 'center'}),
                   
                   dbc.Row([dbc.Col(dcc.Graph(figure=pre_gisa),
                               style={"margin-left": "10rem"})
                       ], #style={"margin-left": "10rem"}
                          ),
                   dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                            dbc.Col(html.A('GISA', 
                                     href='https://samapriya.github.io/awesome-gee-community-datasets/projects/gisa/',
                                   target="_blank", className='src-link-style'))
                           ],
                          className='src-style-urb')
                 ]
        return pre_urb
    elif pathname == '/pre/pop':
        pre_pop = [dbc.Row([html.Div([html.H3('(2020)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                   dbc.Row([html.Div(html.H5(density_txt, style = {'text-align':'center'})
                                    )
                           ], style={'margin-bottom': '2rem', 
                                                 'text-align': 'center',
                                                'align-content': 'center'}),
                   dbc.Row([dbc.Col(dcc.Graph(figure=pre_map_pop),
                               #width=6
                               )
                       ]),
                   dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                            dbc.Col(html.A('WorldPop', 
                                     href='https://www.worldpop.org',
                                   target="_blank", className='src-link-style'))
                           ],
                          className='src-style-pp')
                 ]
        return pre_pop
    elif pathname == '/pre/lan':
        pas_lan = [dbc.Row([html.Div([html.H3('(2021)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),
                   
                   dbc.Row([dbc.Col(dcc.Graph(figure=pre_bar_land))],
                          style={'margin-bottom': '1rem'}),
                   dbc.Row([dbc.Col(dcc.Graph(figure=pre_map_land))]),
                            
                   dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                            dbc.Col(html.A('Dynamic World V1', 
                                     href='https://developers.google.com/earth-engine/datasets/catalog/GOOGLE_DYNAMICWORLD_V1#description',
                                   target="_blank", className='src-link-style'))
                           ],
                          className='src-style-pl')
                       ]
        return pas_lan

    elif pathname == '/fut':
        fut = [dbc.Row([html.Div([html.H3('(2020-2040)', style = {'text-align':'center'}
                                             )],
                                    style = SUBTITLE_YEAR)
                                     ]),

               dbc.Row([dbc.Col(dcc.Graph(figure=sleuth_fast_map),
                               style={"width": "26rem",
                                     'margin-bottom': '1rem'}
                               ),
                        dbc.Col(dcc.Graph(figure=sleuth_usual_map),
                               style={"width": "26rem",
                                     'margin-bottom': '1rem'}
                               )
                       ]),
               
               dbc.Row([dbc.Col(dcc.Graph(figure=sleuth_slow_map),
                               style={"width": "26rem"}
                               ),
                        dbc.Col(dcc.Graph(figure=sleuth_graph),
                               style={"width": "26rem"}
                               )
                       ]),
               
               dbc.Row([dbc.Col(html.P('Fuente:'), className='src-txt-style'),
                            dbc.Col(html.A('Sleuth Python', 
                                     href='https://github.com/gperaza/sleuth_python',
                                   target="_blank", className='src-link-style'))
                           ],
                          className='src-style-pp')
              ]
        return fut



if __name__ == "__main__":
    app.run_server()

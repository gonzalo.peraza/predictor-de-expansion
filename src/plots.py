#!/usr/bin/env python
# coding: utf-8

# In[30]:
from ast import literal_eval
import base64
import json
from rasterio.enums import Resampling
from shapely.geometry import Polygon
import degree_of_urbanization as du
import geemap.plotlymap as geemap
import plotly.graph_objects as go
import rpy2.robjects as robjects
from collections import Counter
from pyproj import Transformer
import plotly.express as px
import data_sources as dts
from PIL import ImageDraw
from PIL import ImageFont
import contextily as ctx
from pathlib import Path
from numpy import matlib
import geopandas as gpd
import rioxarray as rxr
import rasterio as rio
from PIL import Image
import pandas as pd
import xarray as xr
import stats as sts
import geemap as gp
import numpy as np
import requests
import rasterio
import warnings
import plotly
import boto3
import sys
import os
import re
import ee

sys.path.append('./sleuth_python')
import spread as spd

warnings.filterwarnings('ignore')

stringKey = "eyd0eXBlJzogJ3NlcnZpY2VfYWNjb3VudCcsICdwcm9qZWN0X2lkJzogJ3ByZWRpY3Rvci1kZS1leHBhbnNpb24nLCAncHJpdmF0ZV9rZXlfaWQnOiAnYWRjMDc0NmQxZjc0YjFiN2Q1NzJmN2NjYTA4MmE5ZTI1ZGJhNmRlNCcsICdwcml2YXRlX2tleSc6ICctLS0tLUJFR0lOIFBSSVZBVEUgS0VZLS0tLS1cbk1JSUV2UUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktjd2dnU2pBZ0VBQW9JQkFRQ1d2ZkdXdWhJYlM5a1lcbm1XZ2xOZW9YRmx1WDBiZkErcnI0dnNKN2xmK2RvcS84NW9XUkxTMDJ2Rnd3ZzhsamN3VUdNRDhSc2RHQU8yOENcbm4zMjVPcDl3MFFpQnltRkliR09aZGxPNmlMNzkyWHB1eDVnR1k5RFJlcTVwNEpqY3RITXR0MWVodHBiYVdsbVlcbkZSdUp6eXRnME1QT1dPdWxtUSs1bnU3N0JPVWxwRUJ2dVB6clFQQmVONnhyb25SVkR2cStoeFBNMUFhTmg0OC9cbnI0YnVrWFNuUWdPVkdHSHhjNTBXWG5zQjl4SHRxRzEvYmJrcWpETGdEU1ZSUUhBM2FPbzRWUGE4R2ppZUhGTEdcbmNZTjVmT2xVYUlIVWpmbWpXT1o0eTZ1akZVTHdXRDNzdWRlRGlkWHVKWisrOHhKMlI2b1BoNXVBTjBMQllYeWVcbndOMEprenVoQWdNQkFBRUNnZ0VBQVRZd1pwS052SjlaZjIyVWxTK250MWUxaWkrVVBnRVRSdXZWOUIzeFFWbXFcbllnL256UWVRdWNRelFRRnBoQXJycnlCWGtodGg3ZGxuSG1nblg4UVQvZTZ6aTVpSSs1R0J6a2V4cTFuaW9DSW9cbldPZ3lPdGlWK0NodEZicUprVnBDenBXa1VZOVRvSU1jcUlEQW1UVUdPaDBLSlRndUxRLzJoNFZQL04ralBiclVcblk0c3NTa1U2a0R0Nm5BM2NOTDVaUnVvVkJlSFdadDkyV1lxblFhYk1GYUVGM3ZLTHROQ2FDTTBnc01GVmRnRmlcblVUcVJtVStYUXFBc0VRTVRVTHRENVltN3QyN2dZeXNVV0g4Q1BWU2VTUmU1dFQzTktyT2p3RGxjSUpVM001cE9cbjVSUW5rblc2TWtINDd0T3VBSG1KaEV2UXhOcjNwcmRDOU9zdWoweGZBUUtCZ1FEVHB0WUVFVWF2VXdwLzZpN3RcblNHU1hEWkFTMnV4R0IyUlB5YUV0UTJIZk84UVlRQkhnaGI3c3RQbVdOeThDcDBrNFBEU2g4dUtBRDFTbTNxNjBcbjc1Nk5IRXM0ZjBmVmlzaHNRbEhQUjJUcFUvTUJIQWpGazVvR045NlR6cmFwb09qNHNaTDcrendJNHhqd09vUVRcbnhONThmZEkvb0JTcVNlcDZhZW9nSW0xSlFRS0JnUUMyVTl5Tm1taWwyK0EvQkUrVTdoQmUwMFgyVlBlaFpyZXVcbmdGSUpsSHRNajhrVXkzK2pNN1lPNHQxR05BQWFjdmt1aHBxcm1GdWJRekovaUU3TEczS3Fvb1o0aGNyTTFNYkRcbk9oc1lIbEhGM2hKc1dNZ1JTeVdvMGJCSFRKNndQUnJxdkVCbC91OXRya0FWcFhiMGFhd1ZLMzdITmNXbkJHVTFcbkdxdmN3Mkg2WVFLQmdIcStXNGw2VjJyZ0ZzdStEVDd3NTkwdU1XcXQ2YTVuSlFhY29VdDFzM0haNEV4TUE5SVlcbjNuTzZOTENvQ2l4WXhiN0dNNlBrRHZWRGJzMWpnU0JQZ3J6Rk50NXAra2ZlTjRPSkpwYTJDZERETGI5cithNVRcbmZsU3kyL01PUTZLMlBjWFVmYkNDci90YmNLQUkzNFh6ZXkvcGJDQVgvMFFPbjl3a1JuSWlaUXpCQW9HQUtVY0JcbkZwblBlVVUzRm0wdWF5UWVWcWwwSEZtT0dsODcxR2krcWVhUXAzdWg1WEJRdkN6MzJaVG96WHoyVFFhY2ZUa0NcbkNUa2IvQ0dSYkNmNjFxYW5KaFJYWnkrN0F2Wms3VXZaV0hOOHhkUUp6YmVueTZwOG95U2R5UGFDc3BtUzMzT3hcbkRLWlRwRHdmekFiQzBJQ2Y4Y2NBRTYrTHlNMEk4S1hTSk0raS9lRUNnWUVBczRsbTd1dGllUmxQbTVWRzVmOWRcbnF0LzBQbUxmMno2Ny9pSk9vTjFpVzNIK2huTWd0WUQrOTA4eVArRGNkNjg2emdwUEtHQ2Z3NGZDYzNYL3UrZi9cbm10MHF1OGR4RitlV0FqREd4THljdDFrSHJ4dUo5VW9PeEMyV0ZVbmNFdngwNVlvWUpRZHRRTG9uRVcwWHVZYWVcbiswTzlxSkQrcUNCQS8rczY1bGdRbDRvPVxuLS0tLS1FTkQgUFJJVkFURSBLRVktLS0tLVxuJywgJ2NsaWVudF9lbWFpbCc6ICdwcmVkLWV4cC1kZWF1bHRAcHJlZGljdG9yLWRlLWV4cGFuc2lvbi5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbScsICdjbGllbnRfaWQnOiAnMTAwMDM1NDg4MDI0NjYxMTgzNjQwJywgJ2F1dGhfdXJpJzogJ2h0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbS9vL29hdXRoMi9hdXRoJywgJ3Rva2VuX3VyaSc6ICdodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbicsICdhdXRoX3Byb3ZpZGVyX3g1MDlfY2VydF91cmwnOiAnaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vb2F1dGgyL3YxL2NlcnRzJywgJ2NsaWVudF94NTA5X2NlcnRfdXJsJzogJ2h0dHBzOi8vd3d3Lmdvb2dsZWFwaXMuY29tL3JvYm90L3YxL21ldGFkYXRhL3g1MDkvcHJlZC1leHAtZGVhdWx0JTQwcHJlZGljdG9yLWRlLWV4cGFuc2lvbi5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSd9"

dataKey = base64.b64decode(stringKey).decode("utf-8")

python_dict = literal_eval(dataKey)

privateKey = json.dumps(python_dict, sort_keys=True)

directory = Path('./data')
cities_fua = gpd.read_file(directory / 'output/cities/cities_fua.gpkg')
cities_uc = gpd.read_file(directory / 'output/cities/cities_uc.gpkg')

s3Client = boto3.client('s3')  
service_account = 'pred-exp-deault@predictor-de-expansion.iam.gserviceaccount.com'
credentials = ee.ServiceAccountCredentials(service_account, None, privateKey)

ee.Initialize(credentials)

############################################### DOWNLOAD DATA ###############################################  

def download_data(country, city):
    ''' 
    This function downloads every data source required to create the dashboard
    -----------------
    
    Parameters:
        - city (str): city to download the data for
        - country (str): country to download the data for
    -----------------
    
    Returns:
         None
    '''
    #This variable is used globally for all the other functions
    global datadir
    
    option = '{}-{}'.format(country,city)
    datadir = directory / f'cache/{option}/'
    
    bbox, uc, fua = du.get_bbox(city, country, datadir, buff=10)
    
    dts.download_rasters(country, city, directory, s3Client, buff=10)
    du.download_rasters_s3(bbox, datadir)
    spd.temp_driver(datadir)

###################################### LAND USE GRAPHS AND STATS (past and present) #########################################  

def land_graph(country, city):
    
    ''' 
    The purpose of this function is to create the stats cards and 
    the area chart for the 'cobertura de suelo' in past, as well 
    as the horizontal bar chart 'cobertura de suelo' in present
    from the dashboard.
    
    This works with DynamicWorld data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - area (plotly figure): area chart
        - land_bar (plotly figure): horizontal bar chart
        - card_land_txt (dict): dictionary with the card's text 
        - card_land_num (dict): dictionary with the card's stats 
    '''
    #Preprocessing of the dataframe
    land_dir = datadir / 'historic_landcover.csv'
    land_data = pd.read_csv(land_dir)
    
    land_data.drop('Unnamed: 0', inplace=True, axis=1)
    land_data = land_data.rename(columns = dts.sources['DynamicWorld']['class_dict'])
    land_data = land_data.rename(columns = {'null': 'Desconocido'})
    land_data.set_index('year', inplace=True)
    land_data = land_data*0.0001
    
    #Area chart of the land use for all the years
    area = px.area(land_data, x=land_data.index, y=land_data.columns, 
                   color_discrete_map=dts.sources['DynamicWorld']['colors'],
                   title='Cobertura de suelo por año')
    
    area.update_traces(mode='markers+lines')
    
    area.update_layout(xaxis_dtick = 'M1',
                      yaxis_title = 'km2',
                      xaxis_title = 'Año',
                      legend_title = 'Tipo de suelo:',
                      font_size = 17,
                     font_color = 'rgb(142, 136, 131)',
                     font_family = 'Franklin Gothic Book',
                     title_font_size =  25,
                     title_font_color = 'rgb(99, 95, 93)',
                     title_font_family = 'ITC Franklin Gothic Std',
                     title_xanchor = 'center',
                     title_yanchor = 'top',
                     title_x = 0.45,
                     title_y = .9,
                     paper_bgcolor = 'white',
                     plot_bgcolor = 'white',
                     legend=dict(y= .85))
    
    area.update_xaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
    area.update_yaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
    
    area.add_annotation(
        x=1.2,
        y=1.2,
        xref="paper", 
        yref="paper",
        text="Doble click en el <br> tipo de suelo <br> para verlo",
        showarrow=False,
        font=dict(family="ITC Franklin Gothic Std",
            size=17,
            color="rgb(99, 95, 93)"),
        align="center",
        bordercolor="#c7c7c7",
        borderwidth=2,
        borderpad=4,
        )
    
    #Statistic of land types 
    card_land_txt = {'Agua': 'No aplica.',
                    'Árboles': 'No aplica.',
                    'Césped/Pasto': 'No aplica.',
                    'Vegetación inundada': 'No aplica.',
                    'Cultivos': 'No aplica.',
                    'Arbusto y matorral': 'No aplica.',
                    'Urbanización': 'No aplica.',
                    'Descubierto': 'No aplica.', 
                    'Nieve y hielo': 'No aplica.'}
    
    card_land_num = {'Agua': 'No aplica.',
                    'Árboles': 'No aplica.',
                    'Césped/Pasto': 'No aplica.',
                    'Vegetación inundada': 'No aplica.',
                    'Cultivos': 'No aplica.',
                    'Arbusto y matorral': 'No aplica.',
                    'Urbanización': 'No aplica.',
                    'Descubierto': 'No aplica.', 
                    'Nieve y hielo': 'No aplica.'}
    
    for (colname, colval) in land_data.iteritems():
        
        #Calculate change per land type
        amount = colval.values[-1]/colval.values[0]
    
        if amount < 1:
            word = 'disminución'
        elif amount > 1:
            word = 'incremento'

        if colname in card_land_txt.keys():
            card_land_txt[colname] = '{} en su cobertura del suelo.'.format(word)
            card_land_num[colname] = amount

    #Preprocessing of dataframe for the present (most recent) year
    present_land = land_data.iloc[-1:]
    present_land.reset_index(drop=True)
    df_land = present_land.T
    df_land = df_land.reset_index()
    df_land = df_land.rename(columns={'index': 'suelo', 2021: 'km2'})
    df_land = df_land.sort_values(by='km2', ascending=False)
    
    
    #Horizontal bar chart for present data
    land_bar = px.bar(df_land, y='suelo', x='km2', title="Cobertura de suelo", 
                  text_auto='.2s', orientation='h', color='suelo',
                  color_discrete_map=dts.sources['DynamicWorld']['colors']
                 )

    land_bar.update_traces(textfont_size=17, 
                           textangle=0, 
                           textposition="outside", 
                           cliponaxis=False
                          )
    
    
    land_bar.update_layout(yaxis_title = ' ',
                           xaxis_title = 'km2',
                           font_size = 17,
                            font_color = 'rgb(142, 136, 131)',
                            font_family = 'Franklin Gothic Book',
                            title_font_size =  25,
                            title_font_color = 'rgb(99, 95, 93)',
                            title_font_family = 'ITC Franklin Gothic Std',
                            title_xanchor = 'left',
                            title_yanchor = 'top',
                            title_x = 0.45,
                            title_y = .9,
                           legend_title=" ",
                            paper_bgcolor = 'white',
                            plot_bgcolor = 'white',
                           legend=dict(y= 1.1, #x=0.5
                                      )
                      )
    
    land_bar.update_xaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
    land_bar.update_yaxes(showgrid=False)
    
    
    return area, land_bar, card_land_txt, card_land_num

        
############################## POPULATION, URBAN AND DENSITY GRAPHS AND STATS (past) ######################################  
def urbanization_df(city, country):
    ''' 
    The function creates (if not previously created)
    two dataframes that are return from the full_run
    function from the degree_of_urbanization.py
    
    This works with Landscan and GISA 2 data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - dfstats (dataframe): 
        - dflargets (dataframe): 
    '''
    dfstats_path = datadir / 'dou_stats.csv'
    dflargest_path = datadir / 'dou_largest.csv'
    
    #Check if any of the dataframes exists if not then creates, saves and reads them
    if dfstats_path.exists() == False or dflargest_path.exists() == False:
        print('Downloading urbanization csv...')
        bbox, uc, fua = du.get_bbox(city, country, datadir, buff=10)
        du.full_run(bbox, datadir)
        print('Done.')
        
        dfstats = pd.read_csv(dfstats_path)
        dflargest = pd.read_csv(dflargest_path)
        
        return dfstats, dflargest
    
    # If already exists then only reads them
    else:
        dfstats = pd.read_csv(dfstats_path)
        dflargest = pd.read_csv(dflargest_path)
        
        return dfstats, dflargest
    
def pop_past_graphs(city, country):
    ''' 
    The function creates the population visuals
    for the past section. These are three graphs:
    urbanization, population and population 
    density.
    
    This works with Landscan and GISA 2 data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - figures (list): list of the three graphs in json
        format 
    '''
    
    df_stats, df_largest = urbanization_df(city, country)
    
    spline_df = 4
    r_smooth_spline = robjects.r['smooth.spline'] #extract R function# run smoothing function
    
    #Get the real data poitns por urb, pop and density
    x = df_largest.year.values
    y1 = df_largest.Pob.values
    y2 = df_largest.Area.values
    y3 = df_largest.Pop_density.values
    
    #Convert the data points in R objects
    r_x = robjects.FloatVector(x)
    r_y1 = robjects.FloatVector(y1)
    r_y2 = robjects.FloatVector(y2)
    r_y3 = robjects.FloatVector(y3)
    
    x_smooth = np.linspace(2000, 2019)
    
    #Create the smooth lines for the tendency
    rspline1 = r_smooth_spline(x=r_x, y=r_y1, df=spline_df)
    rspline2 = r_smooth_spline(x=r_x, y=r_y2, df=spline_df)
    rspline3 = r_smooth_spline(x=r_x, y=r_y3, df=spline_df)
    ySpline1 = np.array(robjects.r['predict'](rspline1, robjects.FloatVector(x_smooth)).rx2('y'))
    ySpline2 = np.array(robjects.r['predict'](rspline2, robjects.FloatVector(x_smooth)).rx2('y'))
    ySpline3 = np.array(robjects.r['predict'](rspline3, robjects.FloatVector(x_smooth)).rx2('y'))
    
    #List of lists with the three different data
    data = [
        [y1, ySpline1,
        'Población',
        'Población urbana'],
        
        [y2, ySpline2,
        'Área urbana',
        'Superficie construida en km'],
        
        [y3, ySpline3,
        'Densidad de población',
        'Densidad']
        ]
    
    #Iterate in the list to create the three graphs and save them in a list
    figures = []
    for d in data:
        fig = go.Figure()
        fig.add_trace(go.Scatter(x=x, y=d[0],
                            mode='markers', line_color='rgba(169,169,169, 0.7)', marker=dict(size=8), 
                                 name='Población'))
        fig.add_trace(go.Scatter(x=x_smooth, y=d[1],
                            mode='lines', line_color='#2F5C97', line_width=2, 
                                 name='Tendencia'))
        
            
        fig.update_layout(title= d[2], xaxis_title = 'Año',
                                    yaxis_title = d[3],
                                    font_size = 17,
                                     font_color = 'rgb(142, 136, 131)',
                                     font_family = 'Franklin Gothic Book',
                                     title_font_size =  25,
                                     title_font_color = 'rgb(99, 95, 93)',
                                     title_font_family = 'ITC Franklin Gothic Std',
                                     title_xanchor = 'left',
                                     title_yanchor = 'top',
                                     title_x = 0.22,
                                     title_y = .95,
                                     paper_bgcolor = 'white',
                                     plot_bgcolor = 'white',
                                      margin= dict(l=20, r=20),
                                      showlegend=False)
            
        fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
        fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)', range=[0,  (np.max(d[0])*1.05)]) #rangemode="tozero")
        
        figures.append(fig.to_json())
    
    return figures

def pop_past_stats(city, country):
    ''' 
    The function calculates the change of 
    population, urbanization and population
    density. 
    
    This works with Landscan and GISA 2 data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - cards_info (dict): the first three elements are
        the exact change in number and the last three the 
        text metioning if they increased of decreased.
    '''
    
    df_stats, df_largest = urbanization_df(city, country)
    
    data = ['Area', 'Pob', 'Pop_density']
    info = []
    
    for dat in data:
        amount = df_largest[dat].iloc[-1]/df_largest[dat].iloc[0]
        info.append(amount)
        
        if amount < 1:
            word = 'disminuyó'
        elif amount > 1:
            word = 'incrementó'
        else:
            word = 'se mantuvo'
        
        info.append(word)
        
    cards_info = {'diff_urb': info[0],
             'diff_pop': info[2],
             'diff_den': info[4],
             'urb_txt': f'{info[1]} la superficie construida.', 
             'pop_txt': f'{info[3]} la población.', 
             'den_txt': f'{info[5]} la densidad poblacional.', 
            }
    
    return cards_info

########################################## DENSITY POPULATION TXT (present)  ############################################

def density_landscan(country, city):
    ''' 
    Function to get the most receant data for 
    the population density. This is meant to 
    be in the present -> population.
    
    This works with Landscan and GISA 2 data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - density_txt (str): a string with the most 
        recent data for population density.
    '''
    
    df_stats, df_largest = urbanization_df(city, country)
    present_density = round(df_largest['Pop_density'].iloc[-1], 2)
    
    density_txt = f'La densidad de población es de {present_density} (habitante por km2) en el 2019.'
    
    return density_txt

################################################ URBANIZATION TXT (present) ##############################################

def urbanization_txt(country, city):
    ''' 
    Function to get the most receant data for 
    the urbanization. This is meant to be in 
    the present -> urbanization.
    
    This works with Landscan and GISA 2 data.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - urbanization_txt (str): a string with the most 
        recent data for urbanization.
    '''

    df_stats, df_largest = urbanization_df(city, country)
    present_urb = round(df_largest['Area'].iloc[-1], 2)
    
    urbanization_txt = f'La superficie construida es de {present_urb} km2 en el 2019.'
    
    return urbanization_txt
    
########################################## PRESENT MAP: POPULATION AND LAND USE ############################################

def present_map(src, country, city):
    ''' 
    The goal of this function is to create a map
    based on the input 'src' with Landscan (population)
    or DynamicWorld (land use). Both through plotly geemap.
    
    This works with Landscan or DynamicWorld data.
    
    -----------------
    
    Parameters:
        - src (str): which data source to create the map for
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - Map (plotly geemap map): a map with a specific 
        layer (DynamicWorld or Landscan)
    '''
  
    # UC and FUA
    uc = cities_uc.loc[(cities_uc.country == country) &
                       (cities_uc.city == city)]
    fua = cities_fua.loc[(cities_fua.country == country) &
                       (cities_fua.city == city)]


    # Map Creation
    Map = geemap.Map()
    
    #Controls list (menu items to add on the map)
    controls = [
    'drawline',
    'drawopenpath',
    'drawclosedpath',
    'drawcircle',
    'drawrect',
    'eraseshape']
    
    #Add the controls list
    Map.add_controls(controls)

    # Center on city
    poly_uc = uc.geometry.values[0]
    poly_fua = fua.geometry.values[0]
    point = poly_fua.centroid
    Map.set_center(point.y, point.x, zoom=10)

    # Add expanded bounding box
    bbox = dts.get_roi(fua, buff=7, buff_type='km')
    
    if src == 'Landscan':
        
        #Get Landscan tif url from the s3
        url = 'http://tec-expansion-urbana-p.s3.amazonaws.com/landscan_global/landscan-global-2020-colorized.tif'
        
        #Add layer with opacity 
        Map.add_cog_layer(url, opacity=0.7)
        Map.set_center(point.y, point.x, zoom=10)
        
        return Map
    
    elif src == 'DynamicWorld':

        ###################### OLD PART ######################

         dynamic_image = dts.load_dw(bbox = bbox.geometry[0], year = 2021, clip = False, reproj=False)
         
         Map.addLayer(dynamic_image, dts.sources['DynamicWorld']['vis_params'], 'DynamicWorld' + ' ' + str(2021))
         
         return Map

        ###################### OLD PART ######################

        #A function in data_sources.py already creates the geemap map for DynamicWorld

        # -> New Part -> return dts.map_dw(Map, bbox_ee = bbox.geometry[0], year=2021, clip=False, reproj = False)
    
########################################## GISA MAP PREDICTION (present) ############################################

def gisa_pre_map():
    ''' 
    It creates a static plotly map with GISA 2021 data 
    from the model prediction.
    
    This works with GISA_2021 tiff (model prediction).
    
    -----------------
    
    Parameters:
        None
    -----------------
    
    Returns:
        - fig (plotly map): a map of the gisa 2021 prediction.
    '''
    
    #Open the GISA 2021 tif prediction
    gisa_2021 = rxr.open_rasterio(datadir / 'gisa-2021.tif').sel(band=1)
    
    #Get coords to create a basemap
    w = gisa_2021.coords['x'].min().item()
    s = gisa_2021.coords['y'].min().item()
    e = gisa_2021.coords['x'].max().item()
    n = gisa_2021.coords['y'].max().item()
    ctx.bounds2raster(w, s, e, n, 'basemap_2021.tif', zoom=10, source=ctx.providers.Stamen.TonerLite, ll=True);
    basemap = rxr.open_rasterio('basemap_2021.tif').sel(band=[1,2,3]).rio.reproject_match(gisa_2021, resampling=rio.enums.Resampling(2)).transpose('y','x','band')
    
    #Merge the basemap with GISA 2021 values where will it be drawn in a coral color
    merge = basemap.values.copy()
    merge[np.where(gisa_2021.values > 0)] = (250, 128, 114)
    
    #Create the map with plotly
    fig = px.imshow(merge, labels=dict(x="", y=""))

    fig.update_layout(title_text = 'Superficie construida', 
                          title_x = 0.5,
                         font_size = 17,
                        font_color = 'rgb(142, 136, 131)',
                        font_family = 'Franklin Gothic Book',
                        title_font_size =  25,
                        title_font_color = 'rgb(99, 95, 93)',
                        title_font_family = 'ITC Franklin Gothic Std',
                        paper_bgcolor = 'white',
                        plot_bgcolor = 'rgb(245, 243, 242)',
                     width=800, height=800,
                      margin=dict(l=20, r=20, t=50, b=20))
    
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)
    
    return fig

############################################# GISA SLIDER (past) #########################################

def gisa_slider(country, city):
    ''' 
    GISA map animation with urbanization stats in the title.
    
    This works with GISA (2000-2019) tiffs.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - fig (plotly map animation): a map animation from the 
        gisa 2000 to 2019 data.
    '''
    
    #Calculate total urb per year
    df_stats, df_largest = urbanization_df(city, country)
    urb_km = df_largest[['Area']]
    urb_km_list = urb_km.values.tolist()
    urb_km_list = [item for sublist in urb_km_list for item in sublist]

    #Make list of years for slider
    year_list = list(range(2000, 2020))
    
    #Geemap sets max width/height to 768
    max_dim = 768
    
    #Use a gisa (2019) raster as a reference to reduce resolution
    gisa_reference = rxr.open_rasterio(datadir / 'gisa-2019.tif').sel(band=1)
    resolution_dst = max(gisa_reference.rio.resolution()) / max_dim * max(gisa_reference.shape)
    
    #Reproject to reduce resolution and load gisa rasters into a list of xarrays
    gisa_list = [rxr.open_rasterio(datadir / f'gisa-{year}.tif').sel(band=1).rio.reproject(dst_crs=4326, 
                resolution=resolution_dst)
                 for year in year_list]
    
    #Create xarray with year dimension
    gisa_xr = xr.concat(gisa_list, dim=pd.Index(year_list, name='year'))
    
    #Select limit coordinates to create the basemap
    w = gisa_xr.coords['x'].min().item()
    s = gisa_xr.coords['y'].min().item()
    e = gisa_xr.coords['x'].max().item()
    n = gisa_xr.coords['y'].max().item()
    ctx.bounds2raster(w, s, e, n, 'basemap.tif', zoom=10, source=ctx.providers.Stamen.TonerLite, ll=True);
    basemap = rxr.open_rasterio('basemap.tif').sel(band=[1,2,3]).rio.reproject_match(gisa_xr, resampling=rio.enums.Resampling(2)).transpose('y','x','band')
    
    #Merge the basemap with gisa
    gisa_ar = gisa_xr.values
    map_list = []
    for g in gisa_ar:
        merge = basemap.values.copy()
        merge[np.where(g > 0)] = (250, 128, 114)
        map_list.append(merge)
        
    #Change the list to and array and finally to an xarray
    map_ar = np.stack(map_list)
    map_xr = xr.DataArray(map_ar, dims=['year', 'y', 'x', 'band'])
    map_xr.coords['year'] = year_list
    
    #Create slider based on year
    fig = px.imshow(map_xr, aspect='equal', animation_frame='year',
                   labels=dict(x="", y=""), width=map_ar.shape[2], height=map_ar.shape[1])
    
    fig.update_layout(title_x = 0.5,
                     font_size = 17,
                    font_color = 'rgb(142, 136, 131)',
                    font_family = 'Franklin Gothic Book',
                    title_font_size =  25,
                    title_font_color = 'rgb(99, 95, 93)',
                    title_font_family = 'ITC Franklin Gothic Std',
                    paper_bgcolor = 'white',
                    plot_bgcolor = 'white',
                     margin=dict(l=20, r=20, t=50, b=20))
    
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)
    
    fig.layout.updatemenus[0].buttons[0].args[1]['frame']['duration'] = 500
    #fig.layout.updatemenus[0].buttons[0].args[1]['transition']['duration'] = 10
    
    for button in fig.layout.updatemenus[0].buttons:
        button['args'][1]['frame']['redraw'] = True

    for k in range(len(fig.frames)):
        fig.frames[k]['layout'].update(title_text=f'Superficie construida en {year_list[k]} es de {round(urb_km_list[k], 2)} km2')
        
    return fig

############################################# LANDSAT GIF (past) #########################################

def landsat_animation(city, country):
    ''' 
    Landsat GIF created with a library from geemap.
    
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - gif_str (str): the path for the landsat animation.
    '''
    
    #Create the path for the landsat animation
    gif_path = datadir / 'landsat_timelapse.gif'
    gif_str = str(gif_path)
    
    #Check if the path exists if not then creates it
    if gif_path.exists() == False:
        
        #Get coords for the selected city, country
        fua = cities_fua.loc[(cities_fua.country == country) &
                                 (cities_fua.city == city)]
        bbox = dts.get_roi(fua, buff=0, buff_type='km').geometry[0]
        bbox_ee = ee.Geometry.Polygon(
               [t for t in zip(*bbox.exterior.coords.xy)])
        
        #Create the animation with geemap landsat_timelapse
        gp.landsat_timelapse(roi=bbox_ee, out_gif=gif_str, 
                         start_year=1990, end_year=2020, 
                         frames_per_second=2,
                         title=f'{country}-{city}',
                         font_size = 30,
                        bands=['Red', 'Green', 'Blue'],
                         apply_fmask=True,
                        fading=False)
        return gif_str
    
    else:
        return gif_str
    
#################################################### SLEUTH MAPS (future)  ######################################  
    
def sleuth_map(country, city, src, title):
    ''' 
    Creates a sleuth map with a basemap from GISA 2021
    it works for the three categories of sleuth: slow, 
    usual and fast grow.
    
    This works with sleuth 2040 tiff (from the model 
    prediction)and GISA 2021.
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
        - src (str): which data source to create the map for
        - title (str): title for the map
    -----------------
    
    Returns:
        - fig (plotly map): a map of the 2040 sleuth prediction.
    '''
    
    #Open the 2040 sleuth prediction and the GISA 2021 prediction rasters
    sleuth = rxr.open_rasterio(datadir / src).sel(band=1)
    gisa_2021 = rxr.open_rasterio(datadir / 'gisa-2021.tif').sel(band=1)
    
    #Get the coords to create the basemap
    w = gisa_2021.coords['x'].min().item()
    s = gisa_2021.coords['y'].min().item()
    e = gisa_2021.coords['x'].max().item()
    n = gisa_2021.coords['y'].max().item()
    ctx.bounds2raster(w, s, e, n, 'basemap_2021.tif', zoom=10, source=ctx.providers.Stamen.TonerLite, ll=True);
    basemap = rxr.open_rasterio('basemap_2021.tif').sel(band=[1,2,3]).rio.reproject_match(sleuth, resampling=rio.enums.Resampling(2)).transpose('y','x','band')
    
    #Merge the basemap with the 2040 sleuth data
    merge = basemap.values.copy()
    merge[np.where(sleuth.values > 0)] = (250, 128, 114)
    
    #Create the plotly map
    fig = px.imshow(merge, labels=dict(x="", y=""))

    fig.update_layout(title_text = title, 
                          title_x = 0.5,
                         font_size = 17,
                        font_color = 'rgb(142, 136, 131)',
                        font_family = 'Franklin Gothic Book',
                        title_font_size =  25,
                        title_font_color = 'rgb(99, 95, 93)',
                        title_font_family = 'ITC Franklin Gothic Std',
                        paper_bgcolor = 'white',
                        plot_bgcolor = 'white',
                     width=500, height=500,
                      margin=dict(l=20, r=20, t=50, b=20))
    
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)
    
    return fig

############################################# SLEUTH DATAFRAME (future) #####################################################

def sleuth_df(datadir):
    ''' 
    Creates a dataframe of the sleuth data predictions
    from 2020 to 2040 and slow, usual and fast categories.
    
    This works with sleuth 2040 tiff (from the model 
    prediction)and GISA 2021.
    -----------------
    
    Parameters:
        - datadir (str): the datadir of the city selected 
        to save the dataframe to.
    -----------------
    
    Returns:
        - df (dataframe): a dataframe of the sleuth predictions.
    '''
    
    dfpath = datadir / 'sleuth_projections.csv'
        
    if dfpath.exists() == False:
        
        print('Downloading sleuth projections csv...')
        df = sts.get_sleuth_dataframe(datadir)
        df.to_csv(datadir / 'sleuth_projections.csv', index=False)
        print('Done.')
        return df
        
    else:
        print('Sleuth dataframe already downloaded.')
        return pd.read_csv(dfpath)

################################################# SLEUTH GRAPHS (future) ##################################################
    
def sleuth_graph():
    ''' 
    Creates a line plot graph to compare the slow,
    usual and fast cases of the sleuth predictions.
    
    This works with sleuth 2020 to 2040 predictions.
    -----------------
    
    Parameters:
        None
    -----------------
    
    Returns:
        - fig (plotly graph): a plotly line plot.
    '''
    
    #Preprocessing of the dataframe
    df = sleuth_df(datadir)
    df['mode'] = df['mode'].replace(['slow','fast', 'usual'],['lento','rápido', 'crecimiento inercial'])
    df.columns = ['escenario', 'año', 'no urbanizado', 'área urbanizada']
    
    #Creates the line plot with plotly
    fig = px.line(df, x='año', y='área urbanizada', color='escenario', markers=True, title=' ')
    
    fig.update_layout(xaxis_title = 'Año',
                      yaxis_title = 'Área urbanizada',
                          font_size = 17,
                         font_color = 'rgb(142, 136, 131)',
                         font_family = 'Franklin Gothic Book',
                         title_font_size =  25,
                         title_font_color = 'rgb(99, 95, 93)',
                         title_font_family = 'ITC Franklin Gothic Std',
                         title_xanchor = 'left',
                         title_yanchor = 'top',
                         title_x = 0.22,
                         title_y = .95,
                         paper_bgcolor = 'white',
                         plot_bgcolor = 'white',
                          margin= dict(l=20, r=20),
                      legend_title="Escenarios: ",
                     legend=dict(orientation="h",
                                y= 1.2))
    
    fig.update_xaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
    fig.update_yaxes(showgrid=True, gridwidth=1, gridcolor='rgb(245, 243, 242)')
    
    return fig
################################################# GIF DOWNLOAD (past) ########################################################

def download_landsat_thumb(country, city):
    ''' 
    Download the landsat data locally and does
    a preprocessing to get a better quality
    (without clouds).
    
    This works with landsat tiff from 1990 to 2019.
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - filenames (list): a list of all the landsat paths.
    '''
    
    filenames = []
    year_list = list(range(1990, 2020))
    
    for year in year_list:
        
        print(f'Downloading landsat thumb year {year}...')
    
        # Get region of interest (bounding box)
        cities_fua['burnin'] = 1
        fua = cities_fua.loc[(cities_fua.country == country) &
                                 (cities_fua.city == city)]
        
        bbox = dts.get_roi(fua, buff=0, buff_type='km').geometry[0]
        bbox_ee = ee.Geometry.Polygon([t for t in zip(*bbox.exterior.coords.xy)])
        
        landsat_ee = dts.load_landsat(bbox, year, reproj=False, col=1, clip=True).multiply(0.0001)
        landsat_vis = landsat_ee.visualize(bands=['Red', 'Green', 'Blue'], min=0, max=0.4, 
                                           gamma=[1,1,1]).clip(bbox_ee)
        vis_params={
            "min": 0,
            "max": 255,
            "bands": ["vis-red", "vis-green", "vis-blue"],
            "format": 'jpg',
            'region': bbox_ee,
            'dimensions': 768
        }
        
        url = landsat_vis.getThumbUrl(vis_params)
        r = requests.get(url, stream=True)
        filename = datadir / f'landsat_{year}.jpg'
        
        with open(filename, "wb") as fd:
            for chunk in r.iter_content(chunk_size=1024):
                fd.write(chunk)
                
        filenames.append(str(filename))
                
    return filenames


def draw_year_landsatIMG(country, city):
    ''' 
    Draws the year in each landsat image.
    
    This works with landsat tiff from 1990 to 2019.
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - filenames_drawn (list): a list of all the landsat drawn
        paths.
    '''
    filenames = download_landsat_thumb(country, city)
    
    #Get the font to draw with
    title_font = ImageFont.truetype('/Library/Fonts/Arial.ttf', 40)
    filenames_drawn = []
    
    #For each landsat image open it, draw it, save it and append its path to the list
    for filename in filenames:
        img = Image.open(filename)
        ImageDraw.Draw(img).text((40, 36), filename.split('_')[1][:-4], fill=(0, 0, 0), 
                                 font=title_font) 
        img.save(f'{filename[:-4]}_draw.jpg')
        filenames_drawn.append(f'{filename[:-4]}_draw.jpg')
    
    return filenames_drawn


def make_gif(country, city):
    ''' 
    Creates a Landsat gif.
    
    This works with landsat tiff from 1990 to 2019.
    -----------------
    
    Parameters:
        - city (str): city to create the visuals for
        - country (str): country to create the visuals for
    -----------------
    
    Returns:
        - filenames_drawn (list): a list of all the landsat drawn
        paths.
    '''
    #Get the file path from the landsats drawn
    filenames = draw_year_landsatIMG(country, city)
    
    #Create the path for the landsat GIF
    gif_name = f'{str(datadir)}/landsatGIF_{country}_{city}.gif'

    #Create the GIF by appending them whilist saving them
    frames = [Image.open(image) for image in filenames]
    frame_one = frames[0]
    frame_one.save(gif_name, format="GIF", append_images=frames,
                   save_all=True, duration=400, loop=0)
    
    return gif_name
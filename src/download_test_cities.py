import geopandas as gpd
import ee
import geemap
import data_sources as dts
from shapely.geometry import Polygon
import time
from pathlib import Path
import rioxarray as rxr

# Initialize to personal GEE account
# This uses my personal Google Drive for storage
# Service accounts do not have drive (I think)
ee.Initialize(opt_url='https://earthengine-highvolume.googleapis.com')

data_path = Path('../data/')
cache_path = data_path / 'cache'
cities_uc = gpd.read_file(data_path / 'output/cities/cities_uc.gpkg')
cities_fua = gpd.read_file(data_path / 'output/cities/cities_fua.gpkg')

cities_list = [
    ('Mexico', 'Mérida'),
    ('Mexico', 'Monterrey'),
    # ('Mexico', 'Veracruz'),
    # ('Mexico', 'Querétaro'),
    # ('Mexico', 'Mexico City')
]

task_list = []
buff = 10
for country, city in cities_list:
    # Create directory if missing
    d = cache_path / f'{country}-{city}'
    d.mkdir(parents=False, exist_ok=True)

    # Landsat bbox
    fua = cities_fua.loc[(cities_fua.country == country) &
                         (cities_fua.city == city)]
    bboxl = dts.get_roi(fua, buff=buff, buff_type='km').geometry[0]
    bbox_eel = ee.Geometry.Polygon(
        [t for t in zip(*bboxl.exterior.coords.xy)])
    # Product bbox must be larger to aleviate utm empty bands problem
    bbox = dts.get_roi(fua, buff=buff+5, buff_type='km').geometry[0]
    bbox_ee = ee.Geometry.Polygon(
        [t for t in zip(*bbox.exterior.coords.xy)])

    # Landsat in sent to google drive
    # due to request limits in Earth Engine
    # for year in range(2015, 2022):
    #     landsat = dts.load_landsat(bboxl, year, reproj=True, col=1)
    #     task_list.append(
    #         dts.download_image(
    #             landsat, bbox_eel, year,
    #             folder=f'{country}-{city}',
    #             name=f'landsat-{year}'
    #         ),
    #     )

    # All other products can be requested locally
    # using get download url with a function
    # adapted from geemap
    for year in dts.sources['Copernicus']['temp_cov']:
        fname = f'copernicus-{year}.tif'
        fpath = d / fname
        if fpath.exists():
            print(f'{fname} already exists, skipping.')
            continue
        copernicus = dts.load_src('Copernicus', year, bbox_ee)
        dts.local_download(copernicus, bbox_ee, d, fname)

    for year in dts.sources['WorldPop']['temp_cov']:
        fname = f'worldpop-{year}.tif'
        fpath = d / fname
        if fpath.exists():
            print(f'{fname} already exists, skipping.')
            continue
        worldpop = dts.load_src('WorldPop', year, bbox_ee).unmask(0)
        dts.local_download(worldpop, bbox_ee,
                           d, fname)

    for year in range(1985, 2020):
        fname = f'gisa-{year}.tif'
        fpath = d / fname
        if fpath.exists():
            print(f'{fname} already exists, skipping.')
            continue
        gisa = dts.load_src('GISA', year, bbox_ee)
        dts.local_download(gisa, bbox_ee, d, fname)

    # Slope
    fname = 'slope.tif'
    fpath = d / fname
    if fpath.exists():
        print(f'{fname} already exists, skipping.')
    else:
        slope = dts.load_src('Geomorpho90-slope', 2020, bbox_ee)
        dts.local_download(slope, bbox_ee, d, fname)

    # Water layer
    fname = 'water.tif'
    fpath = d / fname
    if fpath.exists():
        print(f'{fname} already exists, skipping.')
    else:
        water = dts.load_src('OSM_waterLayer', 2021, bbox_ee)
        dts.local_download(water, bbox_ee, d, fname)

    # Protected areas
    fname = 'protected.tif'
    fpath = d / fname
    if fpath.exists():
        print(f'{fname} already exists, skipping.')
    else:
        protected = dts.load_src('WDPA', 2021, bbox_ee)
        dts.local_download(protected, bbox_ee, d, fname)

    # Roads
    # Load a landsat image to match roads projection into
    landsat_path = list(d.glob('landsat*.tif'))[0]
    landsat = rxr.open_rasterio(d / 'landsat-2021.tif')
    lbands = landsat.attrs['long_name']
    landsat = landsat.assign_coords(band=list(lbands))
    landsat.attrs.update(long_name='Landsat')

    road_files = ['roads.tif', 'road_i.tif',
                  'road_j.tif', 'road_dist.tif']
    road_exists = all([(d / f).exists() for f in road_files])
    if road_exists:
        print('All road files already exist, skipping.')
    else:
        roads = dts.load_roads(bbox, d, landsat)
        pass

print(f'{len(task_list)} tasks submitted.')

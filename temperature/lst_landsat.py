import ee
import numpy as np


def bitwise_extract(image, start, end):
    mask_size = ee.Number(1).add(end).subtract(start)
    mask = ee.Number(1).leftShift(mask_size).subtract(1)
    return image.rightShift(start).bitwiseAnd(mask)


def get_mask_landsat(image):
    qa = image.select("QA_PIXEL")
    mask1 = bitwise_extract(qa, 1, 1).eq(0)
    mask2 = bitwise_extract(qa, 3, 3).eq(0)
    mask3 = bitwise_extract(qa, 4, 4).eq(0)
    mask = mask1.bitwiseAnd(mask2).bitwiseAnd(mask3)
    return mask


def get_date_bounds(year, season):
    seasons = {
        "spring": ("03-20", "06-21"),
        "summer": ("06-21", "09-22"),
        "fall": ("09-22", "12-21"),
        "winter": ("12-21", "03-20")
    }

    if season == "winter":
        start = str(year) + "-" + seasons["winter"][0]
        end = str(year + 1) + "-" + seasons["winter"][1]
    else:
        start = str(year) + "-" + seasons[season][0]
        end = str(year) + "-" + seasons[season][1]
    return start, end


def mosaic_duplicate_dates_landsat(collection):
    dates = collection.aggregate_array("DATE_ACQUIRED").getInfo()
    dates_unique = np.unique(dates)
    n = len(dates_unique)
    
    collections_mosaic = [None] * n
    for i, date in enumerate(dates_unique):
        col = collection.filter(ee.Filter.eq("DATE_ACQUIRED", date))
        collections_mosaic[i] = col.mosaic()
        
    return ee.ImageCollection(collections_mosaic)


def get_collection_landsat(collection, bbox, year, season=None):
    if season is None:
        bounds = (str(year), str(year + 1))
    else:
        bounds = get_date_bounds(year, season)
    
    def clip_and_mask(image):
        image = image.clip(bbox)
        mask = get_mask_landsat(image)
        image = image.updateMask(mask)
        return image
    
    filtered = (
        collection
        .filterBounds(bbox)
        .filterDate(bounds[0], bounds[1])
    )
    
    filtered = filtered.select(["QA_PIXEL", "ST_B10"])
    
    col_mosaic = mosaic_duplicate_dates_landsat(filtered)
    col_masked = col_mosaic.map(clip_and_mask)
    
    return col_masked


def kelvin_to_bits_landsat(temp):
    return int(np.round((temp - 149) / 0.00341802))